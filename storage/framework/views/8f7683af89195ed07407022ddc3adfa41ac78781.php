<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Add New Member</title>
	<link rel="stylesheet" type="text/css" href="/assets/bootstrap-3.1.1/css/bootstrap.min.css">
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-3" style="margin-top:50px">
				<h4>ADD NEW MEMBER  <a style="margin-left: 220px" href="displayMembers">Display Members</a> </h4>
				<hr>

		<?php if(Session::get('success')): ?>
			<div class="alert alert-success">
				<?php echo e(Session::get('success')); ?>

			</div>
		<?php endif; ?>

		<?php if(Session::get('fail')): ?>
			<div class="alert alert-danger">
				<?php echo e(Session::get('fail')); ?>

			</div>
		<?php endif; ?>

			<form action="add" method="post">

			<?php echo csrf_field(); ?>
				<div class="form-group">
					<label for="">Name </label>
					<input type="text" class="form-control" name="name" placeholder="Enter Member Name" value="<?php echo e(old('name')); ?>">
					<span style="color: red"><?php $__errorArgs = ['name'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?><?php echo e($message); ?> <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?></span>
				</div>
				<br>
				<div class="form-group">
					<label for="">Email address </label>
					<input type="text" class="form-control" name="email" placeholder="Enter Email Address" value="<?php echo e(old('email')); ?>">
					<span style="color: red"><?php $__errorArgs = ['email'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?><?php echo e($message); ?> <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?></span>
				</div>
				<br>
				<div class="form-group">
					<label for="">Choose a School : </label>
  						<select name="school" id="school" class="form-control" value="<?php echo e(old('school')); ?>">
  							<span style="color: red"><?php $__errorArgs = ['school'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?><?php echo e($message); ?> <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?></span>
    						<option value="International">International School London</option>
    						<option value="Norwegian">Norwegian school</option>
    						<option value="SIS">SIS-LPEBL Kentish Town</option>
    						<option value="EIFA">EIFA International </option>
    						<option value="Dwight">Dwight School</option>
    						<option value="Halcyon">Halcyon International</option>
    					</select>
  				</div>
  				<br>

				<div class="form-group">
					<button type="submit" class="btn btn-primary btn-block"> ADD</button>
				</div>
				
			</form>
		
			</div>
		</div>	
	</div>

</body>
</html><?php /**PATH C:\xampp\htdocs\Laravel_Projects\php_assessment\resources\views/ToucanTech/index.blade.php ENDPATH**/ ?>