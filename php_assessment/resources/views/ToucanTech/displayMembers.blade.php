<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Display Members </title>
	<link rel="stylesheet" type="text/css" href="/assets/bootstrap-3.1.1/css/bootstrap.min.css">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous"></script>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-3" style="margin-top:50px">
				<h4>Displaying All Members :   
				<hr>

		@if(Session::get('success'))
			<div class="alert alert-success">
				{{ Session::get('success')}}
			</div>
		@endif

		@if(Session::get('fail'))
			<div class="alert alert-danger">
				{{ Session::get('fail')}}
			</div>
		@endif

			@csrf
				
				<div class="form-group">
					<label for="">Select a School : </label>
  						<select name="school" id="school" class="form-control" value="{{ old('school')}}">
  							<span style="color: red">@error('school'){{ $message }} @enderror</span>
    						<option value="International">International School London</option>
    						<option value="Norwegian">Norwegian school</option>
    						<option value="SIS">SIS-LPEBL Kentish Town</option>
    						<option value="EIFA">EIFA International </option>
    						<option value="Dwight">Dwight School</option>
    						<option value="Halcyon">Halcyon International</option>
    					</select>
    				<hr>
  				</div>
			<table class="table table-hover">
				<thead>
					<th>Member Name</th>
					<th>Email-ID</th>
					<th>School Name</th>
				</thead>
				<tbody>
					
				@foreach ($list as $item)
					<tr>
						<td>{{ $item ->name }}</td>
						<td>{{ $item ->email }}</td>
						<td data-school="{{ $item ->school_name}}">{{ $item ->school_name}}</td>
					</tr>
					
				@endforeach

				</tbody>

			</table>


			</div>
		</div>	
	</div>
<script type="text/javascript" >
		 $('#school').change(function(){
						
			$('table.table tbody tr').hide();
			$('table.table tbody tr').find('td:eq(2)[data-school="' + $(this).val() + '"]').each(function(){
					$(this).parents('tr:eq(0)').show();
			});
		}).trigger('change');
</script>

</body>
</html>