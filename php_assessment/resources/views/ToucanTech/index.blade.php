<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Add New Member</title>
	<link rel="stylesheet" type="text/css" href="/assets/bootstrap-3.1.1/css/bootstrap.min.css">
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-3" style="margin-top:50px">
				<h4>ADD NEW MEMBER  <a style="margin-left: 220px" href="displayMembers">Display Members</a> </h4>
				<hr>

		@if(Session::get('success'))
			<div class="alert alert-success">
				{{ Session::get('success')}}
			</div>
		@endif

		@if(Session::get('fail'))
			<div class="alert alert-danger">
				{{ Session::get('fail')}}
			</div>
		@endif

			<form action="add" method="post">

			@csrf
				<div class="form-group">
					<label for="">Name </label>
					<input type="text" class="form-control" name="name" placeholder="Enter Member Name" value="{{ old('name')}}">
					<span style="color: red">@error('name'){{ $message }} @enderror</span>
				</div>
				<br>
				<div class="form-group">
					<label for="">Email address </label>
					<input type="text" class="form-control" name="email" placeholder="Enter Email Address" value="{{ old('email')}}">
					<span style="color: red">@error('email'){{ $message }} @enderror</span>
				</div>
				<br>
				<div class="form-group">
					<label for="">Choose a School : </label>
  						<select name="school" id="school" class="form-control" value="{{ old('school')}}">
  							<span style="color: red">@error('school'){{ $message }} @enderror</span>
    						<option value="International">International School London</option>
    						<option value="Norwegian">Norwegian school</option>
    						<option value="SIS">SIS-LPEBL Kentish Town</option>
    						<option value="EIFA">EIFA International </option>
    						<option value="Dwight">Dwight School</option>
    						<option value="Halcyon">Halcyon International</option>
    					</select>
  				</div>
  				<br>

				<div class="form-group">
					<button type="submit" class="btn btn-primary btn-block"> ADD</button>
				</div>
				
			</form>
		
			</div>
		</div>	
	</div>

</body>
</html>