<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

class ToucanTech extends Controller
{
    
    function index(){
    	return view('ToucanTech.index');
    }

    function displayMembers()
    {
        //return $request->input();
        $data = array('list' => DB::table('member')->get());

        return view('ToucanTech.displayMembers',$data);
    }

    function add(Request $request){

    	//return $request->input();

    	$request->validate([
    		'name'=>'required',
    		'email'=>'required|email',
    		'school'=>'required'

    	]);

        $query = DB::table('member')->insert([

            'name'=>$request->input('name'),
            'email'=>$request->input('email'),
            'school_name'=>$request->input('school')
        ]);

        if($query){

            return back()->with('success','Data has been successfully inserted..');
        }
        else{
            return back()->with('fail','something went wrong...');
        }


    }
}