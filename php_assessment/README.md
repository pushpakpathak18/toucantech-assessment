# ToucanTech Assessment/php_assessment/
#**php_assessment**

#Introduction: This project will create a School Model where we can add Members of a school and we also display members based on a particular school.

#Technologies:
Operating System:Windows 8 or above
Bootstrap 3 or 4
PHP 5 or 7
Laravel Framework 8.x

#Table of Contents:
#Project Name: **php_assessment**
#General Info:
    In this project  we have created a school module where we add members of a school which will store the data into Member table. When we click on Display member link it will redirect to display member page where we need to select school and on the basis of that school it will display members of that school.

#Setup:
Step1: Install composer
    composer create-project --prefer-dist laravel/laravel blog
Step 2: Start development server for Laravel
    php artisan serve

If you want to run the project on different port so use this command

    php artisan serve --port=8080 

Step3:Open browser and type :
    http://127.0.0.1:8000/ToucanTech
